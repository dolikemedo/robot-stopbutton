# robot-stopbutton

Stopbutton with Microcontroller in a ROS1 ecosystem

<img src="stopgombschema.png" width="300" height="290" />

There is an ESP8266 Wemos D1 mini in the buttons box.
It is connected as seen in the diagram above.

## HOW TO USE

- Edit the topics for your own use. The button can do anything essentially.

        std_msgs::String str_msg;
        actionlib_msgs::GoalID stop_msg;
        ros::Publisher stopbutton_msg("stopbutton", &str_msg);
        ros::Publisher stopbutton_func("/move_base/cancel", &stop_msg);

the stop_msg is purposefully left uninitialized leaving it an empty object. 
movebase receiving an empty GoalID type message from the /move_base/cancel topic will stop the navigation stack. 

- Edit the 'serve' function for your own use.
- Edit the Wifi authentication information.
- Edit the rosserial socket server information for your own master.
    > since our robot in IoT workshop has a TwoMaster-Edge/CloudControl architechture
    > we use the robot's master and start a rosserial socker server on the robot controller computer.
- Upload the code to the MCU
- Start the rosserial server
- Connect the MCU through USB to any 5V powersupply.


### Credit:

The project was developed in BME - TMIT department - HSN Lab - IoTSmartcity workshop
by Bsc lvl student
Budapest Hungary
