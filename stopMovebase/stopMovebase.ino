#include <ESP8266WiFi.h>
#include <Wire.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <actionlib_msgs/GoalID.h>


    const int STOP = 5;   //D1
    int stateread = 0;
    int state = 0;
    unsigned long lastServe = 0;
    unsigned long lastSpin = 0;
    unsigned long now = millis();
  
///***********Wifi és ROS beállítások*************//

    const char* ssid = "";
    const char* password = "";
    // Set the rosserial socket server IP address
    IPAddress server(192,168,1,202);
    // Set the rosserial socket server port
    const uint16_t serverPort = 11411;
   
    ros::NodeHandle nh;
    // Make a chatter publisher
    std_msgs::String str_msg;
    actionlib_msgs::GoalID stop_msg;
    ros::Publisher stopbutton_msg("stopbutton", &str_msg);
    ros::Publisher stopbutton_func("/move_base/cancel", &stop_msg);

    // Be polite and say hello
    char hello[13] = "hello world!";

//***************************//

void setup_wifi(){
  delay(10);
  // We start by connecting to a WiFi network
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED){
    Serial.println(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  randomSeed(micros());

}

void setup_ros(){
  // Set the connection to rosserial socket server
  nh.getHardware()->setConnection(server, serverPort);
  nh.initNode();

  // Another way to get IP
  Serial.print("IP = ");
  Serial.println(nh.getHardware()->getLocalIP());

  // Start to be polite
  nh.advertise(stopbutton_msg);  
  nh.advertise(stopbutton_func); 
  
}

//***************************//


void serve(int statepar){
  if (nh.connected()){
      Serial.println("Connected");
      if(statepar==1){
        str_msg.data = "button active!";
        stopbutton_msg.publish( &str_msg );
        stopbutton_func.publish( &stop_msg );

      }else{
        str_msg.data = "button released!";
        stopbutton_msg.publish( &str_msg );
      }
    }
  else{
      Serial.println("Not Connected to ROS");
    }
    
//    sprintf(msg, "%d", statepar);
//    Serial.println(msg);
//    client.publish("Rebi/Stop", msg);
}
  
///*************SETUP**************///
   
void setup() {

  Serial.begin(115200);
      
  pinMode(STOP, INPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  
  //**MQTT SETUP**//
  
  setup_wifi();
  setup_ros();  

  delay(2000);
}

//*************LOOP**************//

void loop(){ 
 
  stateread = digitalRead(STOP);
 
  if(stateread == HIGH and state == 0){
    delay(500);
    stateread = digitalRead(STOP);
    while(stateread == HIGH){
      state = 1;
      if(now-lastServe > 600){
          serve(1);  
          lastServe = now;
          Serial.println("Button Pushed!    ");
          Serial.println(state);
          digitalWrite(LED_BUILTIN, LOW);   
        }
      stateread = digitalRead(STOP);
      if(now-lastSpin>1000){
          nh.spinOnce();
          lastSpin = now; 
        } 
      now = millis();
      yield();
      }    
    }
  else if (stateread == LOW and state == 1){
    delay(500);
    stateread = digitalRead(STOP);
    while(stateread == LOW){
      state = 0;
      if(now-lastServe > 2000){
          serve(0);  
          lastServe = now;
          Serial.println("Button Released!  ");
          Serial.println(state);
          digitalWrite(LED_BUILTIN, HIGH);
        }
      stateread = digitalRead(STOP);
      if(now-lastSpin>1000){
          nh.spinOnce();
          lastSpin = now; 
        } 
      now = millis();
      yield();
      }        
    }

    if(now-lastSpin>1000){
      nh.spinOnce();
      lastSpin = now; 
      }
      
    now = millis();
    
}
